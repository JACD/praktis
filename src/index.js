import React from 'react';
import ReactDOM from 'react-dom'; 
import App from './App'; 
import "bootstrap/dist/css/bootstrap.css"; 
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./Reducers/rootReducer";
import { Provider, useSelector } from "react-redux";
import thunk from "redux-thunk"
import {getFirebase, ReactReduxFirebaseProvider, isLoaded} from "react-redux-firebase";
import firebase from "./config/firebaseConfig";
import {createFirestoreInstance} from "redux-firestore";


const store = createStore(rootReducer, applyMiddleware(thunk.withExtraArgument({getFirebase})));

const rrfProps={
	firebase,
	config:{},
	dispatch: store.dispatch,
	createFirestoreInstance
}

function AuthIsloaded({children}){
  const auth = useSelector(state => state.firebase.auth);
  if(!isLoaded(auth))
    return (
<div className="text-center">
<div 
className="spinner-grow text-primary" 
style={{ width: "5rem", height: "5rem",  position: "fixed",
    top: "50%",
    left: "50%",
    color:"darkred"}}
role="status">
<span className="sr-only">Loading.....</span>
</div>
</div>
      );
  return children;
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store = {store} >
    <ReactReduxFirebaseProvider {...rrfProps}>
    <AuthIsloaded>
    <App />
    </AuthIsloaded>
    </ReactReduxFirebaseProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
 