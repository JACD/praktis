import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

  var firebaseConfig = {
    apiKey: "AIzaSyA__xMyRrF8uH8G9LivH7czCszc-ozgcig",
    authDomain: "praktis-5ea26.firebaseapp.com",
    databaseURL: "https://praktis-5ea26.firebaseio.com",
    projectId: "praktis-5ea26",
    storageBucket: "praktis-5ea26.appspot.com",
    messagingSenderId: "181076280310",
    appId: "1:181076280310:web:20562a81c8c77eb2097dd7"
  };

  firebase.initializeApp(firebaseConfig);
  firebase.firestore();
 
 const storage = firebase.storage();

export {
    storage, firebase as default
}
//  export default firebase;