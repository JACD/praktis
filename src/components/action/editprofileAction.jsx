
 export const getProfile = id => {
 return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		firestore
		.collection("Profile")
		.doc(id)
      .get()
      .then(resp => {
        const profile = resp.data();
        dispatch({ type: "PROFILE_FETCHED", profile });  
	      })
	      .catch(err => {
	        console.log(err.message);
	      });
  };
}; 


export const editProfile = id => {
	return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		const authorId = getState().firebase.auth.uid;
		let newData = id;
		firestore
		.collection("Profile")
		.doc(authorId)
		.set(
		{
			...newData
		},
		{merge: true}
		)
		.then(() => {
			console.log(newData);
			dispatch({ 
				type: "EDIT_PPROF",
				newData
			});
		}) 
		.catch(err => {
			dispatch({
				type: "EDIT_PPROF_ERR",
				err
			});
		});
	};
};

// export const setProfilePic = pic => {
//   	return (dispatch, getState, {getFirebase}) => {
// 		const firestore = getFirebase().firestore(); 
// 		const firebase = getFirebase();
// 		const authorId = getState().firebase.auth.uid;
 
//       const imgRef = firebase.storage().ref('profile/');
//       imgRef
//         .put(pic[0])
//         .then(snapshot => {
//           imgRef.getDownloadURL().then(url => {
//             firebase
//               .auth()
//               .currentUser.updateProfile({
//                 photoURL: url
//               })
//               .then(() => {
//                 firebase
//                   .collection("Profile")
//                   .doc(authorId)
//                   .update({
//                     photoURL: url
//                   });
//               })
//               .then(() => {
//               		dispatch({ type: "ProfPic_suc"});
//               });
//           });
//         })
//         .catch(err => {
//           dispatch({ type: "ProfPic_fail"});
//         }); 
//   };
// };

export const setProfilePic = data => {
  return (dispatch, getState, { getFirebase, getFirestore }) => { 
  	const firestore = getFirebase().firestore(); 
    const firebase = getFirebase(); 
    const authorId = getState().firebase.auth.uid; 
      const imgRef = firebase.storage().ref(`profiles/${authorId}`); 
      imgRef
        .put(data)
        .then(snapshot => {
          imgRef.getDownloadURL().then(url => {
            firebase
              .auth()
              .currentUser.updateProfile({
                photoURL: url
              })
              .then(() => {
                firestore
                  .collection("Profile")
                  .doc(authorId)
                  .set({
                    photoURL: url
                  },
                  {merge: true});
              })
              .then(() => { 
              	//dispatch({ type: "ProfPic_suc"});
              	console.log("Success")
              });
          });
        })
        .catch(err => {
            console.log(err.message);
        }); 
  };
};