//import firestore from "firebase/app";

export const addtask = tasks => {
	return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		const authorId = getState().firebase.auth.uid;
		firestore
		.collection("tasks")
		.add({
			...tasks,
			authorId: authorId,
			date: new Date()
		})
		.then(() => {
			dispatch({
				type: "ADD_TASK",
				tasks
			});
		}) 
		.catch(err => {
			dispatch({
				type: "error",
				err
			});
		});
	};
};

export const removetask = task => {
	return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		//const authorId: getState().firebase.auth.uid;
		firestore
		.collection("tasks")
		.doc(task.id)
		.delete()
		.then(() => {
			dispatch({
				type: "REMOVE_TASK",
			});
		}) 
		.catch(err => {
			dispatch({
				type: "REMOVE_TASK_ERR",
				err
			});
		});
	};
};

export const edittask = task => {
	return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		//const authorId: getState().firebase.auth.uid;
		firestore
		.collection("tasks")
		.doc(task.id)
		.set(
		{
			...task,
			checked: !task.checked
		},
		{merge: true}
		)
		.then(() => {
			dispatch({
				type: "EDIT_TASK",
				task
			});
		}) 
		.catch(err => {
			dispatch({
				type: "EDIT_TASK_ERR",
				err
			});
		});
	};
};


export const addProfile = profile => {
	return (dispatch, getState, {getFirebase}) => {
		const firestore = getFirebase().firestore();
		const authorId = getState().firebase.auth.uid;
		firestore
		.collection("Profile")
		.doc(authorId)
		.set({
			...profile,  
			date: new Date(),
			photoURL: ""
		})
		.then(() => {
			dispatch({
				type: "P_SUCCESS",
				profile
			});
		}) 
		.catch(err => {
			dispatch({
				type: "P_ERR",
				err
			});
		});
	};
};