import React, {Component} from "react";
import {signup} from "../action/authaction";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom"; 

class Signup extends Component {
	state = {
		email: "",
		password: "",
	};

handleChange = (e) => {
this.setState({
	[e.target.id]: e.target.value,
});
};

handleSubmit = (e) => {
e.preventDefault()
console.log(this.state);
this.props.signup(this.state);
};

	render() {
	const {uid} = this.props;
	if(uid) return <Redirect to="/profile" />
	return (
<>
<div className="container">
  <div className="row">
    <div className="col">
    </div>
    <div className="col-5">
      <form className="container" style={{ marginTop: "50px" }} onSubmit = {this.handleSubmit}>
<legend><h1>Sign up</h1> </legend>
  <div className="form-group">
    <label htmlFor="exampleInputEmail1">Email address</label>
    <input type="email" className="form-control" id="email" 
    onChange = {this.handleChange} required />
 </div>
  <div className="form-group">
    <label htmlFor="exampleInputPassword1">Password</label>
    <input type="password" className="form-control" id="password" onChange = {this.handleChange} required />
  </div>
  <button type="submit" className="btn btn-primary">Signup</button>
</form>
    </div>
    <div className="col"> 
    </div>
  </div>
</div>

</>
	);
	}
}

const mapStateToProps = (state) => {
	console.log(state);
	const uid = state.firebase.auth.uid;
	return {
	uid: uid
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		signup: (creds) => dispatch(signup(creds))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
