import React from "react"; 
import NavItems from "./NavItems";
import {connect} from "react-redux";

const NavBar = () => {
return (
<> 
<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <a className="navbar-brand" href="#">Praktis</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
<NavItems/>
</nav>
</>
);	
};

//const mapStateToProps = (state) => ({
//	console.log(state);
//)}
export default NavBar;
