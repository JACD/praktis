import React, {Component} from "react"; 
import {connect} from "react-redux";
import {getProfile,editProfile} from "../action/editprofileAction"; 

class EditProfile extends Component {

 state = {
    fname: "",
	lname: "",
	age: "",
	gender: ""
};

 componentDidMount() {
      this.props.getProfile(this.props.auth.uid);

  }
componentDidUpdate(prevProps) { 
     const profileInfo = this.props.profileInfo;  
     if(profileInfo !== prevProps.profileInfo){ 
		this.setState(profileInfo);
	}
  }
 
// handleChange = (e) => {
// this.setState({
// 	[e.target.id]: e.target.value,
// });
// };

 handleChange = e => {
this.setState({
	[e.target.id]: e.target.value,
});
    // this.setState({
    //   profile: {
    //     ...this.state.profile,
    //     [e.target.id]: e.target.value
    //   }
    // });
  };

  handleSubmit = e => { 
	e.preventDefault()	
this.props.editProfile(this.state); 
this.props.history.push('/profile_update');
}

  // handleSubmit = e => {
  //   const { auth } = this.props;
  //   this.setState({ saveProfile: true });
  //   e.preventDefault();
  //   this.props.updateProfile(this.state.profile, auth.uid); 
  // };
 

	render() {
	 const { profileInfo } = this.props; 

	return (
<>
<div className="container"  style={{ marginTop: "50px" }}>
  <div className="row">
    <div className="col">
    </div>
    <div className="col-5">
   <form id="profile" onSubmit={this.handleSubmit}>
  <div className="form-group">
    <label>First Name</label>
    <input type="text" className="form-control" id="fname" placeholder="First Name" defaultValue={profileInfo && profileInfo.fname} onChange = {this.handleChange} />
  </div>
  <div className="form-group">
    <label>Last Name</label>
    <input type="text" className="form-control" id="lname" placeholder="Last Name" defaultValue={profileInfo && profileInfo.lname} onChange = {this.handleChange}/>
  </div>
  <div className="form-group">
    <label>Age</label>
    <input type="number" className="form-control" id="age" placeholder="Age" defaultValue={profileInfo?.age} onChange = {this.handleChange}/>
  </div>
   <div className="form-group">
    <label>Gender</label>
    <select className="form-control" id="gender" onChange = {this.handleChange}>
      <option>{profileInfo && profileInfo.gender}</option>
      <option>Male</option>
      <option>Female</option>
    </select>
  </div>
  <button type="submit" className="btn btn-primary">Save</button>
</form>
    </div>
    <div className="col"> 
    </div>
  </div>
</div>
</>
	);
	}
}
 

const mapStateToProps = state => { 
  return {
    //profile: state.firebase.profile,
    auth: state.firebase.auth,
    profileInfo: state.profile.profile
  }; 
};
 
const mapDispatchToProps = (dispatch) => {
    return {
        getProfile: (id) => dispatch(getProfile(id)),
        editProfile: (id) => dispatch(editProfile(id))
    };  
 };
 

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
