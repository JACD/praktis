import React, {Component} from "react";
import {addProfile} from "../action/action";
import {connect} from "react-redux";

class Profile extends Component {

 state = {
    fname: "",
	lname: "",
	age: "",
	gender: ""
};

handleChange = (e) => {
this.setState({
	[e.target.id]: e.target.value,
});
};

 
 handleSubmit = e => { 
	e.preventDefault()	
this.props.addProfile(this.state);
document.getElementById("profile").reset();
this.props.history.push('/');
}
 

	render() { 
	return (
<>
<div className="container"  style={{ marginTop: "50px" }}>
  <div className="row">
    <div className="col">
    </div>
    <div className="col-5">
   <form id="profile" onSubmit={this.handleSubmit}>
  <div className="form-group">
    <label>First Name</label>
    <input type="text" className="form-control" id="fname" placeholder="First Name" onChange = {this.handleChange} />
  </div>
  <div className="form-group">
    <label>Last Name</label>
    <input type="text" className="form-control" id="lname" placeholder="Last Name" onChange = {this.handleChange}/>
  </div>
  <div className="form-group">
    <label>Age</label>
    <input type="text" className="form-control" id="age" placeholder="Age" onChange = {this.handleChange}/>
  </div>
   <div className="form-group">
    <label>Gender</label>
    <select className="form-control" id="gender" onChange = {this.handleChange}>
      <option>Select</option>
      <option>Male</option>
      <option>Female</option>
    </select>
  </div>
  <button type="submit" className="btn btn-primary">Save</button>
</form>
    </div>
    <div className="col"> 
    </div>
  </div>
</div>
</>
	);
	}
}
 

const mapDispatchToProps = dispatch => {
	return {
		addProfile: addpro => dispatch(addProfile(addpro))
	};
};

export default connect(null, mapDispatchToProps)(Profile);