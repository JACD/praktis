import React, {Component} from "react"; 
import {connect} from "react-redux";
import {getProfile,setProfilePic} from "../action/editprofileAction"; 
import Profilelogo from "../images/avat.png"; 
//import Avatar from 'react-avatar';

class Profileshows extends Component {
  constructor(props) {
    super(props);
    this.imgUpload = React.createRef();
  }

state = {
 imagepreview: ""
}; 

 componentDidMount() {
      this.props.getProfile(this.props.auth.uid); 
  }
 
 handleChange = e => {
this.setState({
  [e.target.id]: e.target.files[0],
});
//console.log(e.target.files);
};
   uploadImg = () => {  
    this.imgUpload.current.click();
  };

  handleSubmit = e => { 
    e.preventDefault();
    this.props.setProfilePic(this.state.imagepreview); 
    //console.log(this.state.imagepreview);
    this.props.history.push('/profile_update');
  };

  render() { 
   const { profileInfo } = this.props;

  return (
  <>
  <div className="container" style={{ marginTop: "50px" }}>
  <div className="row">
    <div className="col-sm-4">
      <form onSubmit={this.handleSubmit}>
<div className="card" style={{ padding: "5%" }}>
  <img src={this.state.imagepreview? URL.createObjectURL(this.state.imagepreview) : profileInfo ? profileInfo.photoURL : null || Profilelogo}
     onClick={this.uploadImg} className="card-img-top" alt="Default profile"/>
       <input
    id="imagepreview"
    type="file"
    accept="image/*"
    onChange={this.handleChange}
    ref={this.imgUpload}
    hidden
    /> 
  <div className="card-bod text-center">
    <h5 className="card-title">Profile Photo</h5> 
     <button type="submit" className="btn btn-primary">Upload</button>
  </div>
</div>
</form>
    </div>
    <div className="col-sm-8"> 
<ul className="list-group">
 <li className="list-group-item text-right"><a href="/editprofile"><span className="material-icons text-danger">
edit
</span></a></li>
  <li className="list-group-item">Fullname: {profileInfo && profileInfo.fname} {profileInfo && profileInfo.lname}</li> 
  <li className="list-group-item">Gender: {profileInfo && profileInfo.gender}</li>
  <li className="list-group-item">Age: {profileInfo && profileInfo.age}</li> 
</ul>
    </div>
  </div>
    </div> 
    </>
  );
  }
}
 

const mapStateToProps = state => { 
  return { 
    auth: state.firebase.auth,
    profileInfo: state.profile.profile
  }; 
};
 
const mapDispatchToProps = (dispatch) => {
    return {
        getProfile: (id) => dispatch(getProfile(id)),
        setProfilePic: pic => dispatch(setProfilePic(pic))
    };  
 };
 

export default connect(mapStateToProps, mapDispatchToProps)(Profileshows);
