import React from "react";
import {Link} from "react-router-dom";
import {signOut} from "../action/authaction";
import {connect} from "react-redux"; 

const NavItems = ({signOut, uid}) => {
	if(uid){
		return(
  <div className="collapse navbar-collapse" id="navbarText">
    <ul className="navbar-nav mr-auto"> 
      <li className="nav-item">
        <Link to="/" className="nav-link">
Todo App
</Link>
      </li>
      <li className="nav-item">
       <Link to="/profile_update" className="nav-link">
Profile
</Link>
      </li> 
    </ul>
    <span className="navbar-text">
       <Link to="/signin" className="nav-link" onClick={signOut}>
LogOut
</Link>
    </span>
  </div>
			)
	}else{
return (
<> 
<Link to="/signup" className="nav-link">
Signup
</Link> 
<Link to="/signin" className="nav-link">
Signin
</Link>
</>
);
}	
};

const mapStateToProps = state => {
	const uid = state.firebase.auth.uid;
	return {
		uid: uid
	}
}

const mapDispatchToProps = (dispatch) =>{
	return {
	signOut: () => dispatch(signOut())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(NavItems);
