import React from "react"; 
import moment from "moment";
import {removetask, edittask} from "../action/action";
import {connect} from "react-redux";
import Check from "./Check";

const Task = ({task, removetask, edittask}) => {

const handleRemove = (task) => {
removetask(task);
};

const handleEdit = (task) => {
edittask(task);
};

	return (
		<> 
       <tr>
      <th scope="row">{task.task}</th>
      <th scope="row">{task.name}</th>
      <td>{moment(task.date.toDate()).calendar()}</td>
      <td>
<Check onClick={()=> handleEdit(task)} checked = {task.checked}/>
      </td>
      <td><span className="material-icons text-danger" onClick = {() => handleRemove(task) } >
delete
</span></td>
    </tr> 
		</>
		);
};

const mapDispatchToProps = dispatch => {
  return {
    removetask: task => dispatch(removetask(task)),
    edittask: (task) => dispatch(edittask(task))
  };
};

export default connect(null, mapDispatchToProps)(Task);