import React, {Component} from "react";
import {addtask} from "../action/action";
import {connect} from "react-redux";

class Addtask extends Component {
	state = {
		task: "",
		name: "", 
		checked: "false"
	};

handleChange = (e) => {
this.setState({
	[e.target.id]: e.target.value,
});
};

handleSubmit = (e) => {
e.preventDefault();
this.props.addtask(this.state);
document.getElementById("addtaskForm").reset();
console.log(this.state);
};

	render() {
	return (
<>
<form id="addtaskForm" className="container" style={{ marginTop: "50px" }} onSubmit = {this.handleSubmit}>
 
  <div className="form-group">
    <label htmlFor="exampleInputEmail1">Task</label>
    <input type="text" className="form-control" id="task" 
    onChange = {this.handleChange} required />
    <p>Name</p>
    <input type="text" className="form-control" id="name" 
    onChange = {this.handleChange} required />
 </div> 
  <button type="submit" className="btn btn-primary">Add</button>
</form>
</>
	);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		addtask: taskto => dispatch(addtask(taskto))
	};
};

export default connect(null, mapDispatchToProps)(Addtask);