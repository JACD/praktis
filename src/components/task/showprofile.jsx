import React from "react"; 
import moment from "moment";
import {connect} from "react-redux";

const ShowProfile = ({profile}) => {


	return (
		<> 
       <tr>
      <th scope="row">{profile.fname}  {profile.lname}</th>
      <th scope="row">{profile.age}</th>
      <th scope="row">{profile.gender}</th> 
      <th scope="row"><span className="material-icons text-danger" style ={{ cursor: "pointer"}}>
edit
</span></th>
    </tr> 
		</>
		);
};
 

export default ShowProfile;