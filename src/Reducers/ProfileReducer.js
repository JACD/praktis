const initState = {
  profile: null
};

const editprofileReducer = (state = initState, action) => {
  switch (action.type) {
    case "PROFILE_FETCHED":
      return {
        ...state,
        profile: action.profile
      };
    default:
      return state;
  }
};

export default editprofileReducer;
