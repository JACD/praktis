import {toast} from 'react-toastify';


const taskReducer = (state = {}, action)=> {
	switch(action.type){
		case "ADD_TASK":{
			toast.success("Added");
			return state;
		}
		case "ADD_TASK_ERR":{
			toast.error("Error");
			return state;
		}
		case "REMOVE_TASK":{
			toast.warn("Remove");
			return state;
		}
		case "REMOVE_TASK_ERR":{
			toast.error("Error");
			return state;
		}
		case "EDIT_TASK":{
			toast.warn("Update");
			return state;
		}
		case "EDIT_TASK_ERR":{
			toast.error("Error");
			return state;
		}
		case "P_SUCCESS":{
			toast.success("SUCCESSFULLY ADDED");
			return state;
		}
		case "P_ERR":{
			toast.error("Error TO ADD");
			return state;
		}
		case "EDIT_PPROF":{
			toast.success("SUCCESSFULLY UPDATE");
			return state;
		}
		case "EDIT_PPROF_ERR":{
			toast.error("SOMETHING WENT WRONG");
			return state;
		}
		default: return state
	}
}

export default taskReducer;