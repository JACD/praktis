import React from 'react';  
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Signin from "./components/auth/Signin";
import Signup from "./components/auth/Signup";
import Profileshows from "./components/layout/Profile_update";
import Profile from "./components/layout/Profile";
import EditProfile from "./components/layout/Edit_profile";  
import Dashboard from "./components/dashboard/Dashboard";
import NavBar from "./components/layout/NavBar";
import { ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


function App() {
  return (
    <>
    <BrowserRouter>
    <ToastContainer/>
       <NavBar />
    <Switch>
    <Route path="/signin" component={Signin} />
    <Route path="/signup" component={Signup} /> 
    <Route path="/profile_update" component={Profileshows} /> 
    <Route path="/profile" component={Profile} />
    <Route path="/editprofile" component={EditProfile} />
    <Route path="/" component={Dashboard} /> 
    </Switch>
    </BrowserRouter>
    </>
  );
}

export default App;
